const initialState = {}

export default function setData(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'SET_DATA' :
            nextState = {
                ...state,
                [action.componentName]: action.data
            }
            return nextState;
        default:
            return state
    }
}