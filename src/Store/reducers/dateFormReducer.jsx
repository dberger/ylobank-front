const initialState = {period: {start: undefined, end: undefined}}

export default function setDateForm(state = initialState, action) {
    let nextState;
    switch (action.type) {
        case 'SET_DATE_FORM' :
            nextState = {
                ...state,
                period: action.period
            }
            return nextState;
        default:
            return state
    }
}