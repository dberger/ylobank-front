import { combineReducers } from 'redux'
import dataReducer from './dataReducer'
import dateFormReducer from './dateFormReducer'

const allReducers = combineReducers({
    dataReducer,
    dateFormReducer
});

export default allReducers