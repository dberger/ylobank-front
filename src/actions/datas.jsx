export const setData = (componentName, data) => {
  return {
    type: "SET_DATA",
    componentName,
    data
  };
};
