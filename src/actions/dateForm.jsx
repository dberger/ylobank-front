export const setDate = period => {
  return {
    type: "SET_DATE_FORM",
    period
  };
};
