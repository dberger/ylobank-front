import axios from "axios";
import { setData } from "../actions/datas";

export default function getAccountsData(info) {
  return function(dispatch, getState) {
    if (info.token) {
      let axiosConfig = {
        headers: { Authorization: "Bearer ".concat(info.token) }
      };

      if (info.requestData) {
        axiosConfig = {
          ...axiosConfig,
          params: info.requestData
        };
      }

      axios
        .get(info.url, axiosConfig)
        .then(response => {
          dispatch(setData(info.componentName, response.data));
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  };
}
