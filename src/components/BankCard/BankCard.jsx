import React, { Component } from "react";
import Icon from "@material-ui/core/Icon";
import Warning from "@material-ui/icons/Warning";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardIcon from "../Card/CardIcon";
import CardFooter from "../Card/CardFooter";
import Danger from "components/Typography/Danger.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { Redirect } from "react-router";
import { connect } from "react-redux";

class BankCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountType: this.props.accountType,
      accountSolde: this.props.accountSolde,
      classes: this.props.classes,
      codeType: this.props.codeType,
      transactions: this.props.transactions,
      isRedirected: false
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.accountSolde !== prevProps.accountSolde) {
      this.setState(state => ({
        ...state,
        accountSolde: this.props.accountSolde
      }));
    }
  }

  handleClick() {
    this.setState(state => ({
      ...state,
      isRedirected: true
    }));
  }

  render() {
    const { accountType, accountSolde, classes, codeType } = this.state;
    const { incomeBankCard, outcomeBankCard } = this.props.dataReducer;

    return (
      <>
        {this.state.isRedirected ? (
          <Redirect
            to={{
              pathname: "/admin/table",
              state: {
                transactions: this.state.transactions,
                accountType,
                accountSolde,
                incomeBankCard,
                outcomeBankCard
              }
            }}
          />
        ) : (
          <GridItem xs={12} sm={6} md={3} onClick={e => this.handleClick(e)}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="success">
                  <Icon>
                    {codeType === "cpt_cheque" ? "credit_card" : "euro_symbol"}
                  </Icon>
                </CardIcon>
                <p className={classes.cardCategory}>{accountType}</p>
                <h3 className={classes.cardTitle}>
                  {accountSolde}
                  <small>€</small>
                </h3>
              </CardHeader>
              <CardFooter stats>
                <div className={classes.stats}>
                  <Danger>
                    <Warning />
                  </Danger>
                  <a
                    href="#pablo"
                    className={classes.cardFooterItems}
                    onClick={e => e.preventDefault()}
                  >
                    Historique{" "}
                    {codeType === "cpt_cheque" ? (
                      ""
                    ) : (
                      <span>Intérêt acquis : 14.06 €</span>
                    )}
                  </a>
                </div>
              </CardFooter>
            </Card>
          </GridItem>
        )}
      </>
    );
  }
}

const mapStateToProps = ({ dataReducer }) => ({
  dataReducer
});

export default connect(mapStateToProps)(BankCard);
