import React, { Component } from "react";
import BankCard from "./BankCard";
import FacebookLoader from "../Loader/FacebookLoader";
import { connect } from "react-redux";
import getAccountsData from "../../api/accountsData";

const URL_GET_ACCOUNTS = process.env.REACT_APP_HOST_API + "/account";
const COMPONENT_CODE = "bankCardRow";

class BankCardRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      classes: this.props.classes,
      accounts: {},
      info: {
        url: URL_GET_ACCOUNTS,
        token: this.props.token,
        componentName: COMPONENT_CODE
      }
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps !== this.props) {
      if (
        this.props.dataReducer[COMPONENT_CODE] &&
        prevState.accounts !== this.props.dataReducer[COMPONENT_CODE]
      ) {
        this.setState(state => ({
          ...state,
          accounts: this.props.dataReducer[COMPONENT_CODE],
          isLoading: false
        }));
      }
    }
  }

  componentDidMount() {
    const { getDatas } = this.props;
    const { info, accounts } = this.state;

    if (!accounts || Object.keys(accounts).length === 0) {
      getDatas(info);
    }
  }

  render() {
    const { isLoading, classes } = this.state;
    return (
      <>
        {isLoading ? (
          <>
            <FacebookLoader nbCard="3" />
          </>
        ) : (
          <>
            {this.state.accounts.map((item, key) => (
              <BankCard
                key={key}
                accountType={item.type}
                accountSolde={item.solde}
                classes={classes}
                codeType={item.codeType}
                transactions={item.transactions}
              />
            ))}
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = ({ dataReducer }) => ({
  dataReducer
});

const mapDispatchToProps = dispatch => {
  return {
    getDatas: info => dispatch(getAccountsData(info))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BankCardRow);
