import React, { Component } from "react";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardFooter from "../Card/CardFooter";
import GridItem from "../Grid/GridItem";
import { Update } from "@material-ui/icons";
import { Doughnut } from "react-chartjs-2";
import CustomRangeDatePicker from "../Datepicker/CustomRangeDatePicker";
import getAccountsData from "../../api/accountsData";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/fr";

// TODO get account current user
const URL_GET_STATS_CREDITS =
  process.env.REACT_APP_HOST_API + "/account/sum-each-debit-type/1";
const COMPONENT_CODE = "creditStatCard";

class CreditStatsCard extends Component {
  chartReference = {};

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: [],
      classes: this.props.classes,
      info: {
        url: URL_GET_STATS_CREDITS,
        token: this.props.token,
        componentName: COMPONENT_CODE,
        requestData: {
          periodStart: moment()
            .startOf("month")
            .format("L"),
          periodEnd: moment()
            .endOf("month")
            .format("L")
        }
      }
    };
    this.getChartReference = this.getChartReference.bind(this);
    this.update = this.update.bind(this);
  }

  getChartReference() {
    return this.chartReference;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps !== this.props) {
      if (this.props.dataReducer[COMPONENT_CODE]) {
        this.setState(state => ({
          ...state,
          data: this.props.dataReducer[COMPONENT_CODE],
          isLoading: false
        }));
        this.getChartReference().props.data.labels = [];
        if (this.state.data.fullLabels && this.state.data.items) {
          for (const value of this.state.data.fullLabels.values()) {
            this.getChartReference().props.data.labels.push(value);
          }
          this.getChartReference().props.data.datasets[0].data = this.state.data.items;
        }
      }

      if (
        this.props.dateFormReducer &&
        this.props.dateFormReducer.period &&
        prevProps.dateFormReducer.period !== this.props.dateFormReducer.period
      ) {
        this.setState(
          state => ({
            ...state,
            info: {
              ...state.info,
              requestData: {
                periodStart: this.props.dateFormReducer.period.start,
                periodEnd: this.props.dateFormReducer.period.end
              }
            }
          }),
          this.update
        );
      }
    }
  }

  update() {
    const { info } = this.state;
    const { getDatas } = this.props;
    getDatas(info);
  }

  componentDidMount() {
    const { getDatas } = this.props;
    const { info, data } = this.state;

    if (!data || Object.keys(data).length === 0) {
      getDatas(info);
    }
  }

  render() {
    const options = {
      maintainAspectRatio: false // Don't maintain w/h ratio
    };
    const data = {
      labels: this.state.data.fullLabels,
      datasets: [
        {
          data: this.state.data.items,
          backgroundColor: [
            "#f44336",
            "#9C27B0",
            "#2196F3",
            "#4CAF50",
            "#FFEB3B",
            "#FF9800",
            "#795548",
            "#607D8B"
          ],
          hoverBackgroundColor: [
            "#f44336",
            "#9C27B0",
            "#2196F3",
            "#4CAF50",
            "#FFEB3B",
            "#FF9800",
            "#795548",
            "#607D8B"
          ]
        }
      ]
    };
    const { classes } = this.state;
    return (
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CustomRangeDatePicker />
          <CardHeader color="info" stats icon>
            <Doughnut
              data={data}
              ref={reference => (this.chartReference = reference)}
              options={options}
            />
          </CardHeader>
          <CardFooter stats>
            <div className={classes.stats}>
              <Update />
              Just Updated
            </div>
          </CardFooter>
        </Card>
      </GridItem>
    );
  }
}

const mapStateToProps = ({ dataReducer, dateFormReducer }) => ({
  dataReducer,
  dateFormReducer
});

const mapDispatchToProps = dispatch => {
  return {
    getDatas: info => dispatch(getAccountsData(info))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreditStatsCard);
