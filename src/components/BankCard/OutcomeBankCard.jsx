import React, { Component } from "react";
import Icon from "@material-ui/core/Icon";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardIcon from "../Card/CardIcon";
import CardFooter from "../Card/CardFooter";
import GridItem from "../Grid/GridItem";
import { Update } from "@material-ui/icons";
import FacebookLoader from "../Loader/FacebookLoader";
import { connect } from "react-redux";
import getAccountsData from "../../api/accountsData";

const URL_GET_OUTCOME =
  process.env.REACT_APP_HOST_API + "/account/sum-outcomes";
const COMPONENT_CODE = "outcomeBankCard";

class OutcomeBankCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      totalSolde: 0,
      classes: this.props.classes,
      info: {
        url: URL_GET_OUTCOME,
        token: this.props.token,
        componentName: COMPONENT_CODE
      }
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps !== this.props) {
      if (this.props.dataReducer[COMPONENT_CODE]) {
        this.setState(state => ({
          ...state,
          totalSolde: this.props.dataReducer[COMPONENT_CODE],
          isLoading: false
        }));
      }
    }
  }

  componentDidMount() {
    const { getDatas } = this.props;
    const { info, data } = this.state;

    if (!data || Object.keys(data).length === 0) {
      getDatas(info);
    }
  }

  render() {
    const { isLoading, classes, totalSolde } = this.state;
    return (
      <>
        {isLoading ? (
          <FacebookLoader nbCard="1" nbMdCol="6" />
        ) : (
          <>
            <GridItem xs={12} sm={12} md={6}>
              <Card>
                <CardHeader color="danger" stats icon>
                  <CardIcon color="danger">
                    <Icon>arrow_downward</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>Dépenses du mois</p>
                  <h3 className={classes.cardTitle}>
                    {totalSolde}‬ <small>€</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Update />
                    Just Updated
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = ({ dataReducer }) => ({
  dataReducer
});

const mapDispatchToProps = dispatch => {
  return {
    getDatas: info => dispatch(getAccountsData(info))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OutcomeBankCard);
