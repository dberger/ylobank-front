import React, { Component } from "react";
import Icon from "@material-ui/core/Icon";
import Card from "../Card/Card";
import CardHeader from "../Card/CardHeader";
import CardIcon from "../Card/CardIcon";
import CardFooter from "../Card/CardFooter";
import GridItem from "../Grid/GridItem";
import { Update } from "@material-ui/icons";
import FacebookLoader from "../Loader/FacebookLoader";
import getAccountsData from "../../api/accountsData";
import { connect } from "react-redux";

const URL_GET_ACCOUNTS = process.env.REACT_APP_HOST_API + "/account/total";
const COMPONENT_CODE = "totalBankCard";

class TotalBankCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      classes: this.props.classes,
      info: {
        url: URL_GET_ACCOUNTS,
        token: this.props.token,
        componentName: COMPONENT_CODE
      },
      total: 0
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps !== this.props) {
      if (this.props.dataReducer[COMPONENT_CODE]) {
        this.setState(state => ({
          ...state,
          total: this.props.dataReducer[COMPONENT_CODE],
          isLoading: false
        }));
      }
    }
  }

  componentDidMount() {
    const { getDatas } = this.props;
    const { info, data } = this.state;

    if (!data || Object.keys(data).length === 0) {
      getDatas(info);
    }
  }

  render() {
    const { isLoading, classes, total } = this.state;
    return (
      <>
        {isLoading ? (
          <FacebookLoader nbCard="1" />
        ) : (
          <>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardHeader color="info" stats icon>
                  <CardIcon color="info">
                    <Icon>account_balance</Icon>
                  </CardIcon>
                  <p className={classes.cardCategory}>Total</p>
                  <h3 className={classes.cardTitle}>
                    {total}
                    <small>€</small>
                  </h3>
                </CardHeader>
                <CardFooter stats>
                  <div className={classes.stats}>
                    <Update />
                    Just Updated
                  </div>
                </CardFooter>
              </Card>
            </GridItem>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = ({ dataReducer }) => ({
  dataReducer
});

const mapDispatchToProps = dispatch => {
  return {
    getDatas: info => dispatch(getAccountsData(info))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TotalBankCard);
