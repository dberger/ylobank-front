import React, {Component} from 'react';
import Card from "../Card/Card";
import GridItem from "components/Grid/GridItem.jsx";

import { Facebook } from 'react-content-loader'
export default class FacebookLoader extends Component {

    constructor(props) {
        super(props);

        this.state = {
            nbCard: this.props.nbCard,
        }
    }

    render() {
        const {nbCard} = this.state;
        return (
           <>
            {[...Array(parseInt(nbCard))].map((x, i) =>
                <GridItem key={i} xs={12} sm={6} md={this.props.nbMdCol ? parseInt(this.props.nbMdCol) : 3}>
                    <Card className="card-loader">
                        <Facebook />
                    </Card>
            </GridItem>
          )}  
          </>          
        );
    }
}
