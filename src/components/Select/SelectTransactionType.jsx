import React, {Component} from 'react';
import axios from "axios";
import ReactLoading from "react-loading";
import Select from 'react-select'
import makeAnimated from 'react-select/animated';

const animatedComponents = makeAnimated();

export default class SelectTransactionType extends Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            isLoading: true,
            placeholder: this.props.placeholder,
            url: this.props.url,
            value: 0
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this._isMounted = true;
        const options = [];
        axios.get(this.state.url)
        .then((response) => {
            response.data.forEach((elt) => {
                options.push({
                    label: elt.name,
                    value: elt.id
                })
            });
            if (this._isMounted) {
                this.setState((state) => ({
                    ...state,
                    options: options,
                    isLoading: false
                }));
        }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
      }


    handleChange(value){
        this.setState((state) => ({
            ...state,
            value: value,
        }));
        this.props.handleChange(value, this.props.valueType);
    }
      
    render() {
        const {options, isLoading, placeholder} = this.state;

        return (
            <>
                {!isLoading ? (
                    <Select
                        closeMenuOnSelect={false}
                        components={animatedComponents}
                        options={options}
                        placeholder={placeholder}
                        value={this.state.value} 
                        onChange={this.handleChange}
                    />
                ) : (
                    <ReactLoading className="react-main-loader" type={"bars"} color={"black"}/>
                )}
            </>
        );
    }
}
