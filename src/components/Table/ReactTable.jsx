import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

export default class ReactTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data
    };
    this.revertSortFunc = this.revertSortFunc.bind(this);
    this.options = {
      defaultSortName: "id", // default sort column name
      defaultSortOrder: "desc" // default sort order
    };
  }

  revertSortFunc = (a, b, order) => {
    // order is desc or asc
    if (order === "asc") {
      return a.id - b.id;
    } else {
      return b.id - a.id;
    }
  };

  render() {
    return (
      <>
        <BootstrapTable
          data={this.state.data}
          striped
          hover
          pagination
          version="4"
          options={this.options}
        >
          <TableHeaderColumn
            isKey
            dataField="id"
            dataSort={true}
            sortFunc={this.revertSortFunc}
          >
            Id
          </TableHeaderColumn>
          <TableHeaderColumn dataField="type">Catégorie</TableHeaderColumn>
          <TableHeaderColumn dataField="amount">Montant</TableHeaderColumn>
        </BootstrapTable>
      </>
    );
  }
}
