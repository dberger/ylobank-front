/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import SelectTransactionType from "components/Select/SelectTransactionType";
import SelectBankAccountType from "components/Select/SelectBankAccountType";
import CardFooter from "components/Card/CardFooter.jsx";
import { TextField } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import axios from "axios";
import { Redirect } from "react-router-dom";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

class AddExpense extends Component {
  constructor(props) {
    super(props);

    this.state = {
      amountSpending: 0,
      debitType: 0,
      accountDebit: 0,
      amountCredit: 0,
      creditType: 0,
      accountCredit: 0,
      token: this.props.token,
      isRedirecting: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeAmountCredit = this.handleChangeAmountCredit.bind(this);
    this.handleChangeAmountDebit = this.handleChangeAmountDebit.bind(this);
    this.submitCredit = this.submitCredit.bind(this);
    this.submitDebit = this.submitDebit.bind(this);
  }

  handleChange(val, type) {
    switch (type) {
      case "debitType":
        this.setState(state => ({
          ...state,
          debitType: val
        }));
        break;
      case "accountDebit":
        this.setState(state => ({
          ...state,
          accountDebit: val
        }));
        break;
      case "accountCredit":
        this.setState(state => ({
          ...state,
          accountCredit: val
        }));
        break;
      case "creditType":
        this.setState(state => ({
          ...state,
          creditType: val
        }));
        break;
      default:
        break;
    }
  }

  handleChangeAmountCredit(val) {
    this.setState(state => ({
      ...state,
      amountCredit: val
    }));
  }

  handleChangeAmountDebit(val) {
    this.setState(state => ({
      ...state,
      amountSpending: val
    }));
  }

  submitDebit() {
    const data = new FormData();
    data.append("account", this.state.accountDebit.value);
    data.append("type", this.state.debitType.value);
    data.append("amount", this.state.amountSpending);
    axios
      .post(process.env.REACT_APP_HOST_API + "/transaction/add/", data, {
        headers: { Authorization: "Bearer ".concat(this.state.token) }
      })
      .then(response => {
        this.setState(state => ({
          ...state,
          isRedirecting: true
        }));
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  submitCredit() {
    const data = new FormData();
    data.append("account", this.state.accountCredit.value);
    data.append("type", this.state.creditType.value);
    data.append("amount", this.state.amountCredit);
    axios
      .post(process.env.REACT_APP_HOST_API + "/transaction/add/", data, {
        headers: { Authorization: "Bearer ".concat(this.state.token) }
      })
      .then(response => {
        this.setState(state => ({
          ...state,
          isRedirecting: true
        }));
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    const { classes, token } = this.props;
    return (
      <div>
        {this.state.isRedirecting ? (
          <Redirect to="/admin/dashboard" />
        ) : (
          <>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <Card>
                  <CardHeader color="success">
                    <h4 className={classes.cardTitleWhite}>
                      Enregister une dépense
                    </h4>
                    <p className={classes.cardCategoryWhite}>
                      Déduire un montant d'un compte bancaire
                    </p>
                  </CardHeader>
                  <CardBody>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={4}>
                        <div className="add-expense__select-account">
                          <SelectTransactionType
                            placeholder="Choisir un type de dépense"
                            url={
                              process.env.REACT_APP_HOST_API +
                              "/transaction-type/debit"
                            }
                            handleChange={this.handleChange}
                            valueType="debitType"
                          />
                        </div>
                      </GridItem>
                      <GridItem xs={12} sm={12} md={3}>
                        <FormControl>
                          <TextField
                            label="Montant"
                            value={this.state.amountSpending}
                            onChange={event => {
                              const { value } = event.target;
                              this.setState({ amountSpending: value });
                            }}
                            margin="normal"
                            variant="outlined"
                          />
                        </FormControl>
                      </GridItem>
                    </GridContainer>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={4}>
                        <div className="add-expense__select-account">
                          <SelectBankAccountType
                            placeholder="Choisir un compte bancaire"
                            url={process.env.REACT_APP_HOST_API + "/account/"}
                            token={token}
                            handleChange={this.handleChange}
                            valueType="accountDebit"
                          />
                        </div>
                      </GridItem>
                    </GridContainer>
                  </CardBody>
                  <CardFooter>
                    <Button color="success" onClick={this.submitDebit}>
                      Valider
                    </Button>
                  </CardFooter>
                </Card>
              </GridItem>

              <GridItem xs={12} sm={12} md={6}>
                <Card>
                  <CardHeader color="info">
                    <h4 className={classes.cardTitleWhite}>
                      Enregister un revenu
                    </h4>
                    <p className={classes.cardCategoryWhite}>
                      Ajouter un montant sur un compte bancaire
                    </p>
                  </CardHeader>
                  <CardBody>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={4}>
                        <div className="add-expense__select-account">
                          <SelectTransactionType
                            placeholder="Choisir un type de revenu"
                            url={
                              process.env.REACT_APP_HOST_API +
                              "/transaction-type/credit"
                            }
                            handleChange={this.handleChange}
                            valueType="creditType"
                          />
                        </div>
                      </GridItem>

                      <GridItem xs={12} sm={12} md={3}>
                        <FormControl>
                          <TextField
                            label="Montant"
                            value={this.state.amountCredit}
                            onChange={event => {
                              const { value } = event.target;
                              this.setState({ amountCredit: value });
                            }}
                            margin="normal"
                            variant="outlined"
                          />
                        </FormControl>
                      </GridItem>
                    </GridContainer>
                    <GridContainer>
                      <GridItem xs={12} sm={12} md={4}>
                        <div className="add-expense__select-account">
                          <SelectBankAccountType
                            placeholder="Choisir un compte bancaire"
                            url={process.env.REACT_APP_HOST_API + "/account/"}
                            token={token}
                            handleChange={this.handleChange}
                            valueType="accountCredit"
                          />
                        </div>
                      </GridItem>
                    </GridContainer>
                  </CardBody>
                  <CardFooter>
                    <Button color="success" onClick={this.submitCredit}>
                      Valider
                    </Button>
                  </CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
          </>
        )}
      </div>
    );
  }
}

AddExpense.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(AddExpense);
